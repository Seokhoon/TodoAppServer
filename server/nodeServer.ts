import * as express from 'express';
import * as path from 'path';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';


const app = express();
app.use(cors());
app.use(bodyParser());

app.use('/', express.static(path.resolve(__dirname, '../../')));

app.get('/todos', (req, res) => {
    res.json(getTodos());
});

app.post('/todo', (req, res) => {
    res.json(addTodo(req.body.title));
});

app.delete('/todo/:todoId', (req, res) => {
    removeTodo(parseInt(req.params.todoId, null));
    res.send(200);
});

app.put('/todos/clearCompleted', (req, res) => {
    clearCompleted();
    res.send(200);
});

app.put('/todos/toggle', (req, res) => {
    toggleTodo(parseInt(req.body.id, null));
    res.send(200);
});

app.put('/todos/toggleAll', (req, res) => {
    toggleAllState(req.body.status);
    res.send(200);
});

// app.put('/todos/toggleAllcompleted', (req, res) => {
//     res.json(toggleAllCompleted());
//     res.send(200);
// });


const server = app.listen(8000, 'localhost', () => {
    const { address, port } = server.address();
    console.log('Listening on %s %s', address, port);
});

class Product {
    constructor (public id: number, public title: string, public price: number) {}
}

class TodoItem implements TodoItem {
  constructor(public id: number, public title: string, public completed: boolean) {}
}

let todos = [
    new TodoItem(0, 'First todo', false), new TodoItem(1, 'Second todo', false), new TodoItem(2, 'Third todo', true)
];


function getTodos (): TodoItem[] {
    return todos;
}

function getTodoById (todoId: number): TodoItem {
    return todos.find(t => t.id === todoId);
}

function clearCompleted() {
    todos = todos.filter(todo => todo.completed === false);
}

function toggleAllState(completed: boolean) {
    todos.forEach(todo => todo.completed = completed);
}

function addTodo(title: string) {
    const newId = todos.length <= 0 ? 0 : Math.max.apply(Math, todos.map(i => i.id)) + 1;
    todos.push(new TodoItem(newId, title, false));
    return newId;
}

function removeTodo(id: number) {
    todos.splice(todos.findIndex(todo => todo.id === id), 1);
}

function toggleTodo(id: number) {
    const todoItem = todos.find(t => t.id === id);
    todoItem.completed = !todoItem.completed;
}

// function toggleAllCompleted() {
//     const list = todos.filter(todo => todo.completed === true);
//     return todos.length > 0 && todos.length === list.length ? true : false;
// }
